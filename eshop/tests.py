from django.test import TestCase
from xmlrpc import client

url = 'http://localhost:8069'
db = 'odifydb'
odooname = 'admin'
odoopassword = 'admin'
common = client.ServerProxy('{}/xmlrpc/2/common'.format(url))
uid = common.authenticate(db, odooname, odoopassword, {})
models = client.ServerProxy('{}/xmlrpc/2/object'.format(url))
# Create your tests here.
class ProductModelTests(TestCase):
    def test_product(self):
        record = models.execute_kw(db, uid, odoopassword,
                                   'product.product', 'search_read', [[['create_uid', '=', 1]]],
                                   {'fields': ['id', 'test', 'description', 'image_medium', 'name', 'standard_price']})
        print ('record:_______', record[6]['name'])

